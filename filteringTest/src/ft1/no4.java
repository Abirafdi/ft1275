package ft1;

import java.util.Scanner;

public class no4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner baca = new Scanner(System.in);
		System.out.print("Masukkan waktu (hh:mm): ");
		String amPm = baca.nextLine();
		if (amPm.length() > 5) {
			int hh = Integer.parseInt(amPm.substring(0, 2));// -->mengambil jam nya (01) kemudian diubah ke integer (1)
			System.out.println();
			if (amPm.charAt(5) == 'A') {
				if (hh == 12) {
					System.out.print("00");
					System.out.print(amPm.substring(2, 5));
				} else {
					System.out.print(amPm.substring(0, 5));
				}
			} else {
				if (hh == 12) {
					System.out.print(amPm.substring(0, 5));
				} else {
					hh = hh + 12;
					System.out.print(hh);
					System.out.print(amPm.substring(2, 5));
				}
			}
		} else {
			int hh = Integer.parseInt(amPm.substring(0, 2));
			if (hh > 12) {
				System.out.print(hh - 12);
				System.out.print(amPm.substring(2, 5));
				System.out.print("PM");
			} else if (hh == 0) {
				System.out.print("12");
				System.out.print(amPm.substring(2, 5));
				System.out.print("AM");
			} else if (hh == 12) {
				System.out.print(hh);
				System.out.print(amPm.substring(2, 5));
				System.out.print("PM");
			} else {
				System.out.print(hh);
				System.out.print(amPm.substring(2, 5));
				System.out.print("AM");
			}
		}
		// String amPm="12:43:22AM";
		baca.close();

	}

}
