package ft1;

import java.util.Scanner;

public class no10 {

	public static void main(String[] args) {
		Scanner tes = new Scanner(System.in);

		System.out.print("Input : ");
		String kalS = tes.nextLine();

		// String kal = kalS.replaceAll(" ", "");
		String[] arr = kalS.replaceAll(" ", "").split("");

		char[] aChar = new char[arr.length];

		String vokal = "";
		String konsonan = "";

		// isi array char
		for (int i = 0; i < aChar.length; i++) {
			aChar[i] = arr[i].toLowerCase().charAt(0);
		}

		for (int i = 0; i < aChar.length; i++) {
			for (int j = 0; j < aChar.length - 1; j++) {
				char wadah = aChar[j];
				if (aChar[j] > aChar[j + 1]) {
					aChar[j] = aChar[j + 1];
					aChar[j + 1] = wadah;
				}

			}
		}
//		for (char c : aChar) {
//			System.out.print(c + " ");
//		}

		String other = "";
		int c;
		char[] alphabet = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
				's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
		for (int i = 0; i < kalS.replaceAll(" ", "").length(); i++) {
			c = 0;
			for (int j = 0; j <= alphabet.length - 1; j++) {
				if (aChar[i] == alphabet[j]) {
					if ((aChar[i]) == 'a' || (aChar[i]) == 'i' || (aChar[i]) == 'u' || (aChar[i]) == 'e'
							|| (aChar[i]) == 'o') {
						vokal = vokal + aChar[i];
						c = 1;
					} else {
						konsonan = konsonan + aChar[i];
						c = 1;
					}
				}

			}
			if (c == 0) {
				other = other + aChar[i];
			}
		}
		System.out.println();
		System.out.println("Vokal : " + vokal.toLowerCase());
		System.out.println("Konsonan : " + konsonan.toLowerCase());
		System.out.println("Other : " + other);
		tes.close();

	}

}
